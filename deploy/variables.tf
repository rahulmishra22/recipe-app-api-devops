variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "email.abc.com"
}

variable "db_username" {
  description = "Username for RDS postgres instannce"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}


